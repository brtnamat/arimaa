import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.game.ApplicationWindow;
import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.pieces.Piece;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.*;

import static org.mockito.Mockito.mockConstruction;

public class PlayTest {
    private Player[] players;
    private Game game;
    private static ApplicationWindow applicationWindow;
    private Player goldPlayer;
    private Player silverPlayer;
    @BeforeAll
    static void beforeAllSetUp() {
        applicationWindow = new ApplicationWindow();
    }
    @BeforeEach
    void setup() {
//        applicationWindow=new ApplicationWindow();
        applicationWindow.startSetUp(false);
        applicationWindow.startGame();
        game=applicationWindow.getArimaaGame();
        players=game.getPlayers();
        goldPlayer=players[0];
        silverPlayer=players[1];
    }
    @Test
    void goldPlayerTurn_movedHare(){
        Assertions.assertTrue(goldPlayer.hasTurn());
        Piece movingPiece=game.getBoard().squares[0][6].getOccupiedBy(); //column, row
        Assertions.assertTrue(movingPiece.canMove());
        Field firstField=movingPiece.getOccupiedField();
        Assertions.assertEquals("Hare",movingPiece.getType());
        Assertions.assertTrue(movingPiece.getOccupiedField().getEastNeighbour().isOccupied());
        movingPiece.getOccupiedField().doClick();
        movingPiece.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(1,goldPlayer.getNumOfTurnMoves());
        Assertions.assertEquals(firstField,movingPiece.getPreviousOccupiedField());
        Assertions.assertTrue(movingPiece.canMove());
        movingPiece.getOccupiedField().doClick();
        movingPiece.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(2,goldPlayer.getNumOfTurnMoves());
        Assertions.assertTrue(movingPiece.canMove());
        Assertions.assertEquals(movingPiece.getOccupiedField().getEastNeighbour(),movingPiece.getPreviousOccupiedField());
        movingPiece.getOccupiedField().doClick();
        movingPiece.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(3,goldPlayer.getNumOfTurnMoves());
        Assertions.assertTrue(movingPiece.canMove());
        Assertions.assertEquals(movingPiece.getOccupiedField().getEastNeighbour(),movingPiece.getPreviousOccupiedField());
        movingPiece.getOccupiedField().doClick();
        movingPiece.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertTrue(movingPiece.canMove());
        Assertions.assertEquals(movingPiece.getOccupiedField().getEastNeighbour(),movingPiece.getPreviousOccupiedField());
        Assertions.assertEquals(movingPiece.getOccupiedField(),game.getBoard().squares[0][2]);
        Assertions.assertFalse(goldPlayer.hasTurn());
        Assertions.assertEquals(4,goldPlayer.getNumOfTurnMoves());
        Assertions.assertEquals(0,silverPlayer.getNumOfTurnMoves());
        Assertions.assertTrue(players[1].hasTurn());
    }
    @Test
    void goldPlayerTurn_removedHare() throws InterruptedException {
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertEquals(0,goldPlayer.getNumOfTurnMoves());
        Piece pieceToRemove=game.getBoard().squares[2][6].getOccupiedBy();;
        Assertions.assertTrue(pieceToRemove.canMove());
        pieceToRemove.getOccupiedField().doClick();
        pieceToRemove.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(1,goldPlayer.getNumOfTurnMoves());
        Assertions.assertNull(pieceToRemove.getOccupiedField());
        Assertions.assertNull(pieceToRemove.getPreviousOccupiedField());
        Assertions.assertEquals(7,goldPlayer.getNumOfRemainingHares());
        Assertions.assertEquals(15,goldPlayer.allPieces.size());
    }
    @Test
    void oneMoveTurnTest(){
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        Piece movingPiece=game.getBoard().squares[0][6].getOccupiedBy(); //column, row
        Assertions.assertTrue(movingPiece.canMove());
        Field firstField=movingPiece.getOccupiedField();
        movingPiece.getOccupiedField().doClick();
        Assertions.assertTrue(movingPiece.getOccupiedField().isTagged());
        movingPiece.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(firstField,movingPiece.getPreviousOccupiedField());
        applicationWindow.getMenu().getChangeTurn().doClick();
        Assertions.assertFalse(goldPlayer.hasTurn());
        Assertions.assertTrue(silverPlayer.hasTurn());
    }
    @Test
    void silverPushingHare(){
        Piece goldPlayerHare= game.getBoard().squares[0][6].getOccupiedBy();
        Piece silverPlayerHare=game.getBoard().squares[0][1].getOccupiedBy();
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        for(int i=0; i<3;i++) {
            goldPlayerHare.getOccupiedField().doClick();
            Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
            goldPlayerHare.getOccupiedField().getWestNeighbour().doClick();
            Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[0][6-1-i]);
        }
        applicationWindow.getMenu().getChangeTurn().doClick();
        Assertions.assertFalse(goldPlayer.hasTurn());
        Assertions.assertTrue(silverPlayer.hasTurn());
        silverPlayerHare.getOccupiedField().doClick();
        Assertions.assertTrue(silverPlayerHare.getOccupiedField().isTagged());
        silverPlayerHare.getOccupiedField().getEastNeighbour().doClick();
        silverPlayerHare.getOccupiedField().doClick();
        Assertions.assertTrue(silverPlayerHare.getOccupiedField().isTagged());
        silverPlayerHare.getOccupiedField().getNorthNeighbour().doClick();
        applicationWindow.getMenu().getChangeTurn().doClick();
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        goldPlayerHare.getOccupiedField().doClick();
        goldPlayerHare.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[0][2]);
        goldPlayerHare.getOccupiedField().doClick();
        goldPlayerHare.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[0][1]);
        applicationWindow.getMenu().getChangeTurn().doClick();
        Piece pushingPiece=game.getBoard().squares[0][0].getOccupiedBy();
        pushingPiece.getOccupiedField().doClick();
        Assertions.assertTrue(pushingPiece.getOccupiedField().isTagged());
        pushingPiece.getOccupiedField().getEastNeighbour().doClick();
        Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
        goldPlayerHare.getOccupiedField().getEastNeighbour().doClick();
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[0][2]);
        Assertions.assertEquals(pushingPiece.getOccupiedField(),game.getBoard().squares[0][1 ]);
    }
    @Test
    void saveAdnLoad(){
        Piece goldPlayerHare= game.getBoard().squares[4][6].getOccupiedBy();
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        goldPlayerHare.getOccupiedField().doClick();
        goldPlayerHare.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(1,goldPlayer.getNumOfTurnMoves());
        Assertions.assertEquals(goldPlayerHare.getPreviousOccupiedField(),game.getBoard().squares[4][6]);
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[4][5]);
        applicationWindow.getMenu().getSaveGame().doClick();
        applicationWindow.getMenu().getLoadGame().doClick();
        Assertions.assertEquals(1,goldPlayer.getNumOfTurnMoves());
        Assertions.assertEquals(goldPlayerHare.getPreviousOccupiedField(),game.getBoard().squares[4][6]);
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[4][5]);
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
    }
    @Test
    void wrongFieldClickedTest_hareDoesntMove(){
        Piece goldPlayerHare= game.getBoard().squares[4][6].getOccupiedBy();
        goldPlayerHare.getOccupiedField().doClick();
        Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
        goldPlayerHare.getOccupiedField().getWestNeighbour().doClick();
        Assertions.assertEquals(1,goldPlayer.getNumOfTurnMoves());
        Assertions.assertEquals(goldPlayerHare.getPreviousOccupiedField(),game.getBoard().squares[4][6]);
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[4][5]);
        goldPlayerHare.getOccupiedField().doClick();
        Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
        goldPlayerHare.getOccupiedField().getEastNeighbour().doClick();
        Assertions.assertFalse(goldPlayerHare.getOccupiedField().getEastNeighbour().isTagged());
        Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
        Assertions.assertEquals(goldPlayerHare.getPreviousOccupiedField(),game.getBoard().squares[4][6]);
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[4][5]);
        game.getBoard().squares[5][6].doClick();
        Assertions.assertFalse(game.getBoard().squares[5][6].isTagged());
        Assertions.assertTrue(goldPlayerHare.getOccupiedField().isTagged());
        Assertions.assertEquals(goldPlayerHare.getPreviousOccupiedField(),game.getBoard().squares[4][6]);
        Assertions.assertEquals(goldPlayerHare.getOccupiedField(),game.getBoard().squares[4][5]);
    }


    @AfterEach
    void endGame(){
        applicationWindow.endGame();
    }
}
