import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.pieces.Cat;
import cz.cvut.fel.pjv.pieces.Elephant;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import static org.mockito.Mockito.*;

public class PieceTest {
    private Piece testedPiece;
    private Field testedPiecesField;
    private Field fieldToGo;
    private Player owner;
    private Game game;
    @BeforeEach
    void setup() {
        game=mock(Game.class);
        owner=new Player(true,game);
        owner.setHasTurn(true);
        testedPiece=new Cat(owner);
        testedPiecesField = mock(Field.class);
        when(testedPiecesField.getNeighbours()).thenReturn(new Field[]{fieldToGo});
        testedPiece.setOccupiedField(testedPiecesField);
        testedPiecesField.setOccupiedBy(testedPiece);
    }
    @Test
    void movePieceTest_fieldToGo(){
        fieldToGo=new Field('a',1,false);
        testedPiece.move(fieldToGo,game);
        Assertions.assertEquals(fieldToGo,testedPiece.getOccupiedField());
    }
    @Test
    void movePieceTest_null(){
        fieldToGo=new Field('a',1,false);
        testedPiece.move(fieldToGo,game);
        Assertions.assertNull(testedPiecesField.getOccupiedBy());
    }
    @Test
    void movePieceTest_removed(){
        fieldToGo=new Field('a',1,true);
        testedPiece.move(fieldToGo,game);
        Assertions.assertNull(testedPiece.getOccupiedField());
    }
    @Test
    void canMoveTest_true(){
        fieldToGo = new Field('a', 1, false);
        Elephant elephantTested = new Elephant(owner);
        fieldToGo.setOccupiedBy(elephantTested);
        elephantTested.setOccupiedField(fieldToGo);
        Assertions.assertTrue(testedPiece.canMove());
    }


}
