import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.pieces.Dog;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.when;

public class CheckTrappedTest {

    static Piece testedPiece;
    static Piece neighbourPiece;
    static Player checkedOwner;
    static Player opposingPlayer;
    static Field neighbourFiled;
    static Field testedPiecesField;
    @BeforeEach
    void setup() {
        neighbourFiled=new Field('a',2,false);
        try (MockedConstruction<Field> field = mockConstruction(Field.class)) {
            testedPiecesField = new Field('a', 1, true);
            when(testedPiecesField.getNeighbours()).thenReturn(new Field[]{neighbourFiled,null,null,null});
            when(testedPiecesField.isTrap()).thenReturn(true);
        }
        checkedOwner =new Player(true,null);
        opposingPlayer=new Player(false,null);
    }
    @Test
    void testCheckedTrapped_false_neighbourIsFriendly(){
        testedPiece=new Hare(checkedOwner);
        neighbourPiece=new Dog(checkedOwner);
        setOccupations(testedPiecesField,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertFalse(testedPiece.checkTrapped());
    }
    @Test
    void testCheckedTrapped_True_neighbourIsEnemy(){
        testedPiece=new Hare(checkedOwner);
        neighbourPiece=new Dog(opposingPlayer);
        setOccupations(testedPiecesField,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertTrue(testedPiece.checkTrapped());
    }
    @Test
    void testCheckedTrapped_True_noNeighbour(){
        testedPiece=new Hare(checkedOwner);
        setOccupations(testedPiecesField,testedPiece);
        Assertions.assertTrue(testedPiece.checkTrapped());
    }

    static void setOccupations(Field field,Piece piece){
        field.setOccupiedBy(piece);
        piece.setOccupiedField(field);
    }
}
