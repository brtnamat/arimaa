import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.pieces.Dog;
import cz.cvut.fel.pjv.pieces.Elephant;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;
import cz.cvut.fel.pjv.players.AIopponent;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.when;

public class AtLestOneNeighbourFreeTest {
    static Field fieldToCheck;
    static Field neighbourFiled;
    static Piece pieceToCheck;
    static AIopponent player;
    @BeforeEach
    void setup(){
        neighbourFiled=new Field('a',2,false);
        player=new AIopponent(null);
        pieceToCheck=new Dog(player);
        try (MockedConstruction<Field> field = mockConstruction(Field.class)) {
            fieldToCheck = new Field('a', 1, true);
            when(fieldToCheck.getNeighbours()).thenReturn(new Field[]{neighbourFiled,null,null,null});
        }
        pieceToCheck.setOccupiedField(fieldToCheck);
        player.allPieces.add(pieceToCheck);
    }

    @Test
    void oneFieldFree_true_nothingOccupied(){
        Assertions.assertTrue(player.atLeastOneNeighbourFree(player.allPieces.get(0)));
    }
    @Test
    void oneFieldFree_false_allPossibleOccupied(){
        neighbourFiled.setOccupiedBy(new Piece(0,null,null));
        Assertions.assertFalse(player.atLeastOneNeighbourFree(player.allPieces.get(0)));
    }
    @Test
    void oneFieldOccupiedByWeaker_false_occupiedByStronger(){
        neighbourFiled.setOccupiedBy(new Elephant(new Player(true,null)));
        Assertions.assertFalse(player.atLeastOneSpotOccupiesWeakerEnemy(player.allPieces.get(0)));
    }
    @Test
    void oneFieldOccupiedByWeaker_true_occupiedByWeaker(){
        neighbourFiled.setOccupiedBy(new Hare(new Player(true,null)));
        Assertions.assertTrue(player.atLeastOneSpotOccupiesWeakerEnemy(player.allPieces.get(0)));
    }
    @Test
    void oneFieldOccupiedByWeaker_false_sameOwner(){
        neighbourFiled.setOccupiedBy(new Hare(player));
        Assertions.assertFalse(player.atLeastOneSpotOccupiesWeakerEnemy(player.allPieces.get(0)));
    }
}
