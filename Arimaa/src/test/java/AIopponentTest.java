
import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.pieces.Dog;
import cz.cvut.fel.pjv.pieces.Elephant;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.when;


public class AIopponentTest {
    static Piece testedPiece;
    static Piece neighbourPiece;
    static Player movingPlayer;
    static Player opposingPlayer;
    static Field neighbourFiled;
    static Field fieldToGoFrom;
    @BeforeAll
    static void setup() {
        neighbourFiled=new Field('a',2,false);
        try (MockedConstruction<Field> field = mockConstruction(Field.class)) {
            fieldToGoFrom = new Field('a', 1, false);
            when(fieldToGoFrom.getNeighbours()).thenReturn(new Field[]{neighbourFiled});
        }
        movingPlayer=new Player(true,null);
        opposingPlayer=new Player(false,null);
    }

    @Test
    void testCanMove_True_IsStronger(){
        testedPiece=new Dog(movingPlayer);
        neighbourPiece=new Hare(opposingPlayer);
        setOccupations(fieldToGoFrom,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertTrue(testedPiece.canMove());
    }
    @Test
    void testCanMove_False_IsWeaker(){
        testedPiece=new Hare(movingPlayer);
        neighbourPiece=new Dog(opposingPlayer);
        setOccupations(fieldToGoFrom,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertFalse(testedPiece.canMove());
    }
    @Test
    void testCanMove_True_IsEqual(){
        testedPiece=new Hare(movingPlayer);
        neighbourPiece=new Hare(opposingPlayer);
        setOccupations(fieldToGoFrom,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertTrue(testedPiece.canMove());
    }
    @Test
    void testCanMove_True_IsWeakerButSameTeam(){
        testedPiece=new Hare(movingPlayer);
        neighbourPiece=new Elephant(movingPlayer);
        setOccupations(fieldToGoFrom,testedPiece);
        setOccupations(neighbourFiled,neighbourPiece);
        Assertions.assertTrue(testedPiece.canMove());
    }
    static void setOccupations(Field field,Piece piece){
        field.setOccupiedBy(piece);
        piece.setOccupiedField(field);
    }
}
