import cz.cvut.fel.pjv.board.Board;
import cz.cvut.fel.pjv.game.ApplicationWindow;
import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.players.Player;
import org.junit.jupiter.api.*;

public class PreparationTest {
    private Player[] players;
    private Game game;
    private static ApplicationWindow applicationWindow;
    private Player goldPlayer;
    private Player silverPlayer;
    private Board board;
    @BeforeAll
    static void beforeAllSetUp() {
        applicationWindow = new ApplicationWindow();
    }
    @BeforeEach
    void setup() {
        applicationWindow.startSetUp(false);
        game = applicationWindow.getArimaaGame();
        players = game.getPlayers();
        board=game.getBoard();
        goldPlayer = players[0];
        silverPlayer = players[1];
    }
    @Test
    void preparationPhaseTest_swappedThreePositions(){
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        board.squares[0][7].doClick();
        Assertions.assertTrue(board.squares[0][7].isTagged());
        board.squares[0][6].doClick();
        Assertions.assertFalse(board.squares[0][7].isTagged());
        Assertions.assertFalse(board.squares[0][6].isTagged());
        Assertions.assertEquals("Hare",board.squares[0][7].getOccupiedBy().getType());
        Assertions.assertEquals("Dog",board.squares[0][6].getOccupiedBy().getType());
        board.squares[0][7].doClick();
        Assertions.assertTrue(board.squares[0][7].isTagged());
        board.squares[4][7].doClick();
        Assertions.assertEquals("Hare",board.squares[4][7].getOccupiedBy().getType());
        Assertions.assertEquals("Horse",board.squares[0][7].getOccupiedBy().getType());
        Assertions.assertFalse(board.squares[0][7].isTagged());
        Assertions.assertFalse(board.squares[0][6].isTagged());
    }
    @Test
    void preparationPhaseTest_wrongFieldClicked(){
        Assertions.assertTrue(goldPlayer.hasTurn());
        Assertions.assertFalse(silverPlayer.hasTurn());
        board.squares[0][7].doClick();
        Assertions.assertTrue(board.squares[0][7].isTagged());
        board.squares[4][3].doClick();
        Assertions.assertTrue(board.squares[0][7].isTagged());
        Assertions.assertFalse(board.squares[4][3].isTagged());
        board.squares[3][6].doClick();
        Assertions.assertEquals("Dog",board.squares[3][6].getOccupiedBy().getType());
        Assertions.assertEquals("Hare",board.squares[0][7].getOccupiedBy().getType());
        Assertions.assertFalse(board.squares[0][7].isTagged());
        Assertions.assertFalse(board.squares[3][6].isTagged());

    }
    @AfterEach
    void endGame(){
        applicationWindow.endGame();
    }
}
