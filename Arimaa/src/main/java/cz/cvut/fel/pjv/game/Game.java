package cz.cvut.fel.pjv.game;
import java.io.*;
import java.util.*;


import cz.cvut.fel.pjv.board.Board;
import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.pieces.*;
import cz.cvut.fel.pjv.players.AIopponent;
import cz.cvut.fel.pjv.players.Player;

public class Game implements Serializable {
    private ApplicationWindow window;
    private final Player[] players = new Player[2];
    private final Board board;
    private Field fieldToGoFrom;
    private Field enemyField;
    private Field firstSwap;
    private Boolean preparationPhase;
    private boolean vsAi;
    private TimerPanel timerPanel;


    public Game(boolean vsAi, Board board, ApplicationWindow window) {
        if (vsAi) {
            players[0] = new Player(true, this);
            players[1] = new AIopponent(this);
        } else {
            players[0] = new Player(true, this);
            players[1] = new Player(false, this);
        }
        this.board = board;
        this.window = window;
        this.preparationPhase = true;
        this.vsAi=vsAi;
    }

    public boolean isVsAi() {
        return vsAi;
    }

    public void setWindow(ApplicationWindow window) {
        this.window = window;
    }

    public TimerPanel getTimerPanel() {
        return timerPanel;
    }

    public void setTimerPanel(TimerPanel timerPanel) {
        this.timerPanel = timerPanel;
    }

    public Board getBoard() {
        return board;
    }

    public Boolean getPreparationPhase() {
        return preparationPhase;
    }

    public void setPreparationPhase(Boolean preparationPhase) {
        this.preparationPhase = preparationPhase;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setFieldToGoFrom(Field fieldToGoFrom) {
        this.fieldToGoFrom = fieldToGoFrom;
    }


    public void setEnemyField(Field enemyField) {
        this.enemyField = enemyField;
    }

    public void changeTurns() {
        Player playerToGo;
        Player playerToStop;
        if (players[0].hasTurn()) {
            playerToGo = players[1];
            playerToStop = players[0];
        } else {
            playerToGo = players[0];
            playerToStop = players[1];
        }
        if (playerToStop.getNumOfTurnMoves() > 0) {
            playerToGo.setHasTurn(true);
            playerToStop.setHasTurn(false);
            playerToGo.turnTimer.start();
            playerToStop.turnTimer.stop();
            playerToGo.setNumOfTurnMoves(0);
            if (fieldToGoFrom != null) {
                untagField(fieldToGoFrom);
            }
        }
        if(playerToGo.getClass() == AIopponent.class){
            ((AIopponent) playerToGo).startPlaying();
        }
    }

    public void checkChange(Player playerToCheck) {
        if (playerToCheck.getNumOfTurnMoves() >= 4) {
            changeTurns();
        }
    }

    public void fillBoard() {
        int rowToAddHare;
        int rowToAddRest;
        for (Player player : players) {
            rowToAddHare = player.getStartingHareRow();
            if (player.isGold()) {
                rowToAddRest = rowToAddHare + 1;
            } else {
                rowToAddRest = rowToAddHare - 1;
            }
            for (int i = 0; i < 8; i++) {
                Field field = board.squares[i][rowToAddHare];
                Hare newHare = new Hare(player);
                player.addIntoAllPieces(newHare);
                newHare.setOccupiedField(field);
                field.setOccupiedBy(newHare);
            }
            for (int i = 0; i < 8; i++) {
                Piece newPiece;
                if (i < 2) {
                    newPiece = new Dog(player);
                } else if (i < 4) {
                    newPiece = new Cat(player);
                } else if (i < 6) {
                    newPiece = new Horse(player);
                } else if (i < 7) {
                    newPiece = new Camel(player);
                } else {
                    newPiece = new Elephant(player);
                }
                player.addIntoAllPieces(newPiece);
                Field field = board.squares[i][rowToAddRest];
                newPiece.setOccupiedField(field);
                field.setOccupiedBy(newPiece);
            }
        }
    }

    public void gameClickHandler(Field field) {
        Piece piece = field.getOccupiedBy();
        if (piece == null && fieldToGoFrom == null) {
            return;
        }
        if (piece != null && piece.canMove() && fieldToGoFrom == null && piece.getOwner().hasTurn()) {
            field.setTagged(true);
            fieldToGoFrom = field;
        } else if (fieldToGoFrom != null && enemyField == null) {
            for (Field fromFieldNeighbour : fieldToGoFrom.getNeighbours()) {
                if (fromFieldNeighbour == field) {
                    if (!field.isOccupied()) {
                        Piece movingPiece = fieldToGoFrom.getOccupiedBy();
                        movingPiece.move(field, this);
                        this.board.updateTraps();
                        return;
                    } else if (field.isOccupied() && !field.getOccupiedBy().getOwner().hasTurn()
                    && field.getOccupiedBy().getStrength()<fieldToGoFrom.getOccupiedBy().getStrength()) {
                        enemyField = field;
                        field.setTagged(true);
                        return;
                    }
                }
            }
        } else if (enemyField != null && fieldToGoFrom != null) {
            Piece pullingOrPushingPiece = fieldToGoFrom.getOccupiedBy();
            if (!pullingOrPushingPiece.canMove()) {
                return;
            }
            for (Field fromFieldNeighbour : fieldToGoFrom.getNeighbours()) {
                if (fromFieldNeighbour == field && !field.isOccupied()) {
                    pullingOrPushingPiece.pull(field, enemyField.getOccupiedBy(), this);
                    this.board.updateTraps();
                    return;
                }
            }
            for (Field enemyNeighbour : enemyField.getNeighbours()) {
                if (enemyNeighbour == field && !field.isOccupied()) {
                    pullingOrPushingPiece.push(field, enemyField.getOccupiedBy(), this);
                    this.board.updateTraps();
                    return;
                }
            }
        }
    }

    public void preparationClickHandler(Field field) {
        Piece piece = field.getOccupiedBy();
        if (piece == null) {
            return;
        }
        if (firstSwap == null && piece.getOwner().hasTurn()) {
            firstSwap = field;
            field.setTagged(true);
        } else if (firstSwap != null && piece.getOwner().hasTurn()) {
            this.pieceSwap(piece, firstSwap.getOccupiedBy());
            this.untagField(firstSwap);
        }

    }

    public void untagField(Field field) {
        field.setBackground(field.getOriginalColor());
        field.setTagged(false);
        if (field == fieldToGoFrom) {
            fieldToGoFrom = null;
            if (enemyField != null) {
                enemyField.setBackground(enemyField.getOriginalColor());
                enemyField = null;
            }
        } else if (field == enemyField) {
            enemyField = null;
        } else if (field == firstSwap) {
            firstSwap = null;
        }
    }

    public void pieceSwap(Piece firstPiece, Piece secondPiece) {
        Field firstTempField = firstPiece.getOccupiedField();
        Field secondTempField = secondPiece.getOccupiedField();
        firstPiece.setPreviousOccupiedField(firstPiece.getOccupiedField());
        secondPiece.setPreviousOccupiedField(secondPiece.getOccupiedField());
        firstPiece.setOccupiedField(secondTempField);
        secondPiece.setOccupiedField(firstTempField);
        secondTempField.setOccupiedBy(firstPiece);
        firstTempField.setOccupiedBy(secondPiece);
    }

    public void win(Player winner, String reason) {
        WinnerScreen winnerScreen;
        for(Player player:players){
            player.turnTimer.stop();
        }
        if (winner.isGold()) {
            winnerScreen = new WinnerScreen(true, reason);
        } else {
            winnerScreen = new WinnerScreen(false, reason);
        }
        window.changeToWinnerScreen(winnerScreen);

    }

    public void lose(Player looser, String reason) {
        WinnerScreen winnerScreen;
        for(Player player:players){
            player.turnTimer.stop();
        }
        if (looser.isGold()) {
            winnerScreen = new WinnerScreen(false, reason);
        } else {
            winnerScreen = new WinnerScreen(true, reason);
        }
        window.changeToWinnerScreen(winnerScreen);
    }

    public void checkIfAllAreFrozen() {
        for (Player player : players) {
            boolean lost=true;
            for (Piece piece : player.getAllPieces()) {
                if (piece.canMove()) {
                    lost=false;
                    break;
                }
            }
            if (lost && player.isGold()) {
                this.lose(player, "Gold player have all pieces frozen");
            } else if(lost && !player.isGold()) {
                this.lose(player, "Silver player have all pieces frozen");
            }
        }

    }
    public void setStartingFields(){
        for (Player player:players){
            for (Piece piece:player.getAllPieces()){
                piece.setStartingField(piece.getOccupiedField());
            }
        }
    }


}

