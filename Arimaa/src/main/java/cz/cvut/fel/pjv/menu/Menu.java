package cz.cvut.fel.pjv.menu;


import cz.cvut.fel.pjv.game.ApplicationWindow;

import javax.swing.*;
import java.awt.*;


public class Menu extends JPanel {
    private ApplicationWindow window;
    private JButton start2PlayersGame;
    private JButton startVsAiGame;
    private JButton loadGame;
    private JButton endGame;
    private JButton endPreparation;
    private JButton changeTurn;
    private JButton giveUp;
    private JButton saveGame;

    public Menu(ApplicationWindow window) {
        this.window=window;
        this.setLayout(new FlowLayout());
        this.addMenuButtons();
        this.setVisible(true);
    }
    public void addMenuButtons(){
        start2PlayersGame = new JButton("vs Person");
        start2PlayersGame.addActionListener(e -> {
            window.startSetUp(false);
        });
        startVsAiGame = new JButton("vs Ai");
        startVsAiGame.addActionListener(e -> {
            window.startSetUp(true);
        });
        loadGame = new JButton("load last game");
        loadGame.addActionListener(e -> {
            window.openLastGame();
        });
        this.add(start2PlayersGame);
        this.add(startVsAiGame);
        this.add(loadGame);
    }
    public void changeToPrepButtons(){
        endPreparation = new JButton("1. player end preparation");
        endPreparation.addActionListener(e -> {
            if(window.getArimaaGame().getPlayers()[1].hasTurn()){
                window.startGame();
            }else {
                endPreparation.setText("2. player end preparation");
                window.getArimaaGame().getPlayers()[0].setHasTurn(false);
                window.getArimaaGame().getPlayers()[1].setHasTurn(true);
            }
        });
        endGame=new JButton("end game");
        endGame.addActionListener(e -> {
            window.endGame();
        });
        this.remove(startVsAiGame);
        this.remove(start2PlayersGame);
        this.remove(loadGame);
        this.revalidate();
        this.repaint();
        this.add(endGame);
        this.add(endPreparation);
    }
    public void changeToGameButtons(){
        changeTurn = new JButton("change turn");
        changeTurn.addActionListener(e -> {
            window.getArimaaGame().changeTurns();
        });
        giveUp=new JButton("give up");
        giveUp.addActionListener(e -> {
            if(window.getArimaaGame().getPlayers()[0].hasTurn()){
                window.getArimaaGame().lose(window.getArimaaGame().getPlayers()[0],"Gold player gave up");
            }else {
                window.getArimaaGame().lose(window.getArimaaGame().getPlayers()[1],"Silver player gave up");
            }
        });
        saveGame = new JButton("save game and quit");
        saveGame.addActionListener(e -> {
            window.saveGame();
        });
        this.remove(endPreparation);
        this.revalidate();
        this.repaint();
        this.add(giveUp);
        this.add(changeTurn);
        this.add(saveGame);
    }
    public void changeToAfterWinButtons(){
        this.remove(changeTurn);
        this.remove(giveUp);
        this.remove(saveGame);
        this.revalidate();
        this.repaint();
    }

    public JButton getChangeTurn() {
        return changeTurn;
    }

    public JButton getSaveGame() {
        return saveGame;
    }

    public JButton getLoadGame() {
        return loadGame;
    }

    public JButton getEndPreparation() {
        return endPreparation;
    }
}
