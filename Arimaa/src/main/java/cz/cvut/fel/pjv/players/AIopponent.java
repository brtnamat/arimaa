package cz.cvut.fel.pjv.players;

import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.pieces.Piece;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;

public class AIopponent extends Player implements Serializable {
    private int numberOfWantedMoves;
    private final Random random=new Random();
    private javax.swing.Timer playTimer ;
    private int numberOfMoves;
    private long timeToPlay=3000;
    private boolean paused;
    public AIopponent(Game participatingGame){
        super(false, participatingGame);
        playTimer=new Timer(3000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                makePlay();
            }
        });
        paused=true;

    }
    public void startPlaying(){
        numberOfWantedMoves=random.nextInt(4)+1;
        numberOfMoves=0;
        start();
    }
    public void pause() {
        paused = true;
        playTimer.stop();
    }
    public void start(){
        paused=false;
        playTimer.start();
    }
    public boolean tryPush(){
        ArrayList<Piece> pushingPieces= getAllPushingPieces();
        ArrayList<Piece> pushablePieces=getAllPushablePieces(pushingPieces);
        if(pushablePieces.size()>0){
            Piece pushedPiece=pushablePieces.get(random.nextInt(pushablePieces.size()));
            Piece pushingPiece=getPushingPiece(pushedPiece);

            ArrayList<Field> possibleFields=getAllPossibleFields(pushedPiece);

            Field fieldToMoveTo=possibleFields.get(random.nextInt(possibleFields.size()));

            pushingPiece.push(fieldToMoveTo,pushedPiece,getParticipatingGame());
            getParticipatingGame().getBoard().updateTraps();
            System.out.println("Opponent pushed");
            return true;
        }
        else{
            return false;
        }
    }
    public boolean tryPull(){
        ArrayList<Piece> pullingPieces= getAllPullingPieces();
        if(getAllPullingPieces().size()>0){
            Piece pullingPiece=pullingPieces.get(random.nextInt(pullingPieces.size()));

            ArrayList<Field> possibleFields=getAllPossibleFields(pullingPiece);
            ArrayList<Piece> allPullablePieces=getAllPullablePieces(pullingPiece);

            Field fieldToMoveTo=possibleFields.get(random.nextInt(possibleFields.size()));
            Piece pieceToPull=allPullablePieces.get(random.nextInt(allPullablePieces.size()));

            pullingPiece.pull(fieldToMoveTo,pieceToPull,getParticipatingGame());
            getParticipatingGame().getBoard().updateTraps();
            System.out.println("Opponent pulled");
            return true;
        }
        else{
            return false;
        }
    }
    public void movePiece(){
        ArrayList<Piece> movablePieces=getAllMovablePieces();
        if(movablePieces.size()==0){
            getParticipatingGame().lose(this,"Silver player can not move with any piece");
            return;
        }
        Piece movingPiece=movablePieces.get(random.nextInt(movablePieces.size()));
        ArrayList<Field> possibleFields=getAllPossibleFields(movingPiece);
        Field fieldToMoveTo=possibleFields.get(random.nextInt(possibleFields.size()));
        movingPiece.move(fieldToMoveTo,getParticipatingGame());
        getParticipatingGame().getBoard().updateTraps();
    }
    public ArrayList<Field> getAllPossibleFields(Piece piece){
        ArrayList<Field> listToReturn=new ArrayList<>();
        Field occupiedField=piece.getOccupiedField();
        for(Field field:occupiedField.getNeighbours()){
            if(field!=null && !field.isOccupied()){
                listToReturn.add(field);
            }
        }
        return listToReturn;
    }
    public ArrayList<Piece> getAllMovablePieces(){
        ArrayList<Piece> listToReturn=new ArrayList<>();
        for(Piece piece:allPieces){
            if(piece.canMove() && atLeastOneNeighbourFree(piece)){
                listToReturn.add(piece);
            }
        }
        return listToReturn;
    }
    public boolean atLeastOneNeighbourFree(Piece piece){
        for(Field neighbour:piece.getOccupiedField().getNeighbours()){
            if(neighbour!=null && !neighbour.isOccupied()){
                return true;
            }
        }
        return false;
    }

    public ArrayList<Piece> getAllPullingPieces() {
        ArrayList<Piece> listToReturn = new ArrayList<>();
        for (Piece piece : allPieces) {
            if (piece.canMove() && atLeastOneSpotOccupiesWeakerEnemy(piece) && atLeastOneNeighbourFree(piece)) {
                listToReturn.add(piece);
            }
        }
        return listToReturn;
    }
    public ArrayList<Piece> getAllPullablePieces(Piece piece) {
        ArrayList<Piece> listToReturn=new ArrayList<>();
        for (Field neighbour : piece.getOccupiedField().getNeighbours()) {
            if (neighbour != null && neighbour.isOccupied()) {
                Piece neighbourPiece = neighbour.getOccupiedBy();
                if (neighbourPiece.getOwner() != piece.getOwner() && neighbourPiece.getStrength() < piece.getStrength()) {
                    listToReturn.add(neighbourPiece);
                }
            }
        }
        return listToReturn;
    }

    public ArrayList<Piece> getAllPushingPieces() {
        ArrayList<Piece> listToReturn = new ArrayList<>();
        for (Piece piece : allPieces) {
            if (piece.canMove() && atLeastOneSpotOccupiesWeakerEnemy(piece)) {
                listToReturn.add(piece);
            }
        }
        return listToReturn;
    }
    public ArrayList<Piece> getAllPushablePieces(ArrayList<Piece> pushedByPieces){
        ArrayList<Piece> listToReturn=new ArrayList<>();
        for(Piece pushedByPiece:pushedByPieces){
            Field[] neighbours=pushedByPiece.getOccupiedField().getNeighbours();
            for(Field neighbourFiled:neighbours){
                if(neighbourFiled!=null && neighbourFiled.isOccupied()) {
                    Piece neighbourPiece = neighbourFiled.getOccupiedBy();
                    if (pushedByPiece.getOwner() != neighbourPiece.getOwner() && atLeastOneNeighbourFree(neighbourPiece)){
                        listToReturn.add(neighbourPiece);
                    }
                }
            }
        }
        return  listToReturn;
    }
    public Piece getPushingPiece(Piece pushedPiece){
        Piece neighbourPiece=null;
        for(Field neighbourField:pushedPiece.getOccupiedField().getNeighbours()){
            if(neighbourField!=null && neighbourField.isOccupied()){
                neighbourPiece=neighbourField.getOccupiedBy();
                if(neighbourPiece.getOwner()!=pushedPiece.getOwner() &&
                        pushedPiece.getStrength()< neighbourPiece.getStrength()){
                    return neighbourPiece;
                }
            }
        }
        return neighbourPiece;
    }

    public boolean atLeastOneSpotOccupiesWeakerEnemy(Piece piece){
        for(Field neighbour:piece.getOccupiedField().getNeighbours()){
            if(neighbour!=null && neighbour.isOccupied()){
                Piece neighbourPiece=neighbour.getOccupiedBy();
                if(neighbourPiece.getOwner()!=piece.getOwner() && neighbourPiece.getStrength()<piece.getStrength()){
                    return true;
                }
            }
        }
        return false;
    }
//    private class AIActionTask extends TimerTask {
//        @Override
//        public void run() {
//            makePlay();
//        }
//    }
    public void makePlay() {
        if(!paused) {
            int typeOfPlay=random.nextInt(3);
            if(typeOfPlay==0){
                movePiece();
            } else if (typeOfPlay==1) {
                if (!tryPull()){
                    movePiece();
                }
            }else {
                if(!tryPush()){
                    movePiece();
                }
            }
            numberOfMoves++;
            timeToPlay=3000;
            if (numberOfMoves >= numberOfWantedMoves) {
                getParticipatingGame().changeTurns();
                pause();
            }
        }
    }

    public void setPlayTimer(Timer playTimer) {
        this.playTimer = playTimer;
    }

}

