package cz.cvut.fel.pjv.board;

import cz.cvut.fel.pjv.pieces.Elephant;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class Field extends JButton implements Serializable {
    private final char character;
    private final int num;
    Field[] neighbours=new Field[4];
    private Field eastNeighbour;
    private Field westNeighbour;
    private Field northNeighbour;
    private Field southNeighbour;
    private boolean occupied;
    private Piece occupiedBy;
    final private boolean trap;
    private Color originalColor;
    private boolean tagged=false;


    public Field (char character, int num,boolean isTrap) {
        this.character = character;
        this.num = num;
        this.trap=isTrap;
    }

    public boolean isTagged() {
        return tagged;
    }

    public void setTagged(boolean tagged) {
        if (tagged){
            this.setBackground(Color.GREEN.brighter());
            this.tagged = true;
        }else {
            this.setBackground(this.originalColor);
            this.tagged = false;
        }
    }

    public Color getOriginalColor() {
        return originalColor;
    }

    public void setOriginalColor(Color originalColor) {
        this.originalColor = originalColor;
    }

    public char getCharacter() {
        return character;
    }

    public int getNum() {
        return num;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public Piece getOccupiedBy() {
        return occupiedBy;
    }

    public void setOccupiedBy(Piece occupiedBy) {
        this.occupiedBy = occupiedBy;
        if (occupiedBy != null) {
            occupied = true;
            this.setIcon(occupiedBy.getIcon());
        } else {
            occupied = false;
            this.setIcon(null);
        }
    }

    public boolean isTrap() {
        return trap;
    }

    public Field getEastNeighbour() {
        return eastNeighbour;
    }

    public void setEastNeighbour(Field eastNeighbour) {
        this.eastNeighbour = eastNeighbour;
    }

    public Field getWestNeighbour() {
        return westNeighbour;
    }

    public void setWestNeighbour(Field westNeighbour) {
        this.westNeighbour = westNeighbour;
    }

    public Field getNorthNeighbour() {
        return northNeighbour;
    }

    public void setNorthNeighbour(Field northNeighbour) {
        this.northNeighbour = northNeighbour;
    }

    public Field getSouthNeighbour() {
        return southNeighbour;
    }

    public void setSouthNeighbour(Field southNeighbour) {
        this.southNeighbour = southNeighbour;
    }

    public Field[] getNeighbours() {
        return neighbours;
    }
}