package cz.cvut.fel.pjv.game;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

public class TurnTimer implements Serializable {
    private JLabel timeLabel = new JLabel();
    private int elapsedTime=1000*60*10;
    private int seconds=0;
    private int minutes=10;
    private boolean running=false;
    private String seconds_string = String.format("%02d", seconds);
    private String minutes_string = String.format("%02d", minutes);
    private Timer timer;
    private Player owner;
    public TurnTimer(Player owner){
        this.owner=owner;
        timeLabel.setText(minutes_string+":"+seconds_string);
        timeLabel.setBounds(100,100,200,100);
        timeLabel.setFont(new Font("Verdana",Font.PLAIN,35));
        timeLabel.setBorder(BorderFactory.createBevelBorder(1));
        timeLabel.setOpaque(true);
        timer= new Timer(1000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                elapsedTime=elapsedTime-1000;
                calculateTime();
                timeLabel.setText(minutes_string+":"+seconds_string);
                if(elapsedTime<=0){
                    owner.ranOutOfTime();
                }
            }

        });
    }
    public void calculateTime(){
        minutes = (elapsedTime/60000) % 60;
        seconds = (elapsedTime/1000) % 60;
        seconds_string = String.format("%02d", seconds);
        minutes_string = String.format("%02d", minutes);
    }
    public void start(){
        timer.start();
        this.running=true;
    }
    public void stop(){
        timer.stop();
        this.running=false;
    }
    public JLabel getTimeLabel() {
        return timeLabel;
    }

    public void setTimeLabel(JLabel timeLabel) {
        this.timeLabel = timeLabel;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(int elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
    public void updateTimeLabel(){
        calculateTime();
        timeLabel.setText(minutes_string+":"+seconds_string);
    }
}
