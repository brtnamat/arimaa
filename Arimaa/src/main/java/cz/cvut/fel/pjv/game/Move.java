package cz.cvut.fel.pjv.game;

import java.io.Serializable;
import java.util.ArrayList;

import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.pieces.Piece;


public class Move implements Serializable {
    final private Field from;
    final private Field to;
    final private Piece movingPiece;

    public Move(Field from, Field to,Piece movingPiece) {
        this.from = from;
        this.to = to;
        this.movingPiece=movingPiece;
    }
}
