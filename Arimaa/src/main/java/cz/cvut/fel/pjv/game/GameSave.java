package cz.cvut.fel.pjv.game;

import cz.cvut.fel.pjv.game.Game;

import java.io.Serializable;

public class GameSave implements Serializable {
    private Game game;

    public GameSave(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }
}