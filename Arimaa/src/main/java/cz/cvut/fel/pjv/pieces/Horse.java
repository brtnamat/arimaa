package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;

public class Horse extends Piece{
    public Horse(Player owner) {
        super(3, "Horse", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/wh.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/bh.png"));
        }
    }
}
