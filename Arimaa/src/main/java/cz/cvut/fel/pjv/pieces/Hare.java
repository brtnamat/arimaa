package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.awt.*;

public class Hare extends Piece{
    private int hereNumber;


    public Hare(Player owner) {
        super(0, "Hare", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/wr.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/br.png"));
        }
    }

}
