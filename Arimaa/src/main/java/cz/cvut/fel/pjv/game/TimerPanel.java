package cz.cvut.fel.pjv.game;

import org.ietf.jgss.GSSManager;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class TimerPanel extends JPanel implements Serializable {
    public TurnTimer turnTimerGold;
    public TurnTimer turnTimerSilver;

    private JLabel turnTimerJLabelGold;
    private JLabel turnTimerJLabelSilver;

    public TimerPanel(Game game) {
        turnTimerGold = new TurnTimer(game.getPlayers()[0]);
        turnTimerSilver = new TurnTimer(game.getPlayers()[1]);
        turnTimerJLabelGold = turnTimerGold.getTimeLabel();
        turnTimerJLabelSilver = turnTimerSilver.getTimeLabel();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel gpLabel=new JLabel("Gold player time: ");
        JLabel spLabel=new JLabel("Silver player time: ");
        gpLabel.setFont(new Font("Arial", Font.BOLD, 20));
        spLabel.setFont(new Font("Arial", Font.BOLD, 20));
        this.add(gpLabel);
        this.add(Box.createRigidArea(new Dimension(10,10)));
        this.add(turnTimerJLabelGold);
        this.add(Box.createRigidArea(new Dimension(10,40)));
        this.add(spLabel);
        this.add(Box.createRigidArea(new Dimension(10,10)));
        this.add(turnTimerJLabelSilver);
        this.setVisible(true);

    }
    public TimerPanel(TurnTimer turnTimerGold, TurnTimer turnTimerSilver){
        this.turnTimerGold=turnTimerGold;
        this.turnTimerSilver=turnTimerSilver;
        turnTimerJLabelGold = turnTimerGold.getTimeLabel();
        turnTimerJLabelSilver = turnTimerSilver.getTimeLabel();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel gpLabel=new JLabel("Gold player time: ");
        JLabel spLabel=new JLabel("Silver player time: ");
        gpLabel.setFont(new Font("Arial", Font.BOLD, 20));
        spLabel.setFont(new Font("Arial", Font.BOLD, 20));
        this.add(gpLabel);
        this.add(Box.createRigidArea(new Dimension(10,10)));
        this.add(turnTimerJLabelGold);
        this.add(Box.createRigidArea(new Dimension(10,40)));
        this.add(spLabel);
        this.add(Box.createRigidArea(new Dimension(10,10)));
        this.add(turnTimerJLabelSilver);
        this.setVisible(true);
    }
}

