package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;

public class Elephant extends Piece{

    public Elephant(Player owner) {
        super(5, "Elephant", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/we.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/be.png"));
        }

    }

}
