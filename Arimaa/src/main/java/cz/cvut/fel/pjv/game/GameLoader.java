package cz.cvut.fel.pjv.game;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class GameLoader implements Serializable {
    public static Game loadGame() {
        Game game = null;
        try {
            FileInputStream fileIn = new FileInputStream("gameState.ser");
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            GameSave gameSave = (GameSave) objectIn.readObject();
            game = gameSave.getGame();
            objectIn.close();
            System.out.println("Game loaded successfully.");
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return game;
    }
}
