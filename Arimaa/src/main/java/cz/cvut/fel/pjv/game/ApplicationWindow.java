package cz.cvut.fel.pjv.game;


import cz.cvut.fel.pjv.board.Board;
import cz.cvut.fel.pjv.menu.Menu;
import cz.cvut.fel.pjv.players.AIopponent;
import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class ApplicationWindow extends JFrame {

    private Menu menu;
    private Board board;
    private Game arimaaGame;
    private TimerPanel timerPanel;
    private WinnerScreen winnerScreen;

    public ApplicationWindow(){
        this.setTitle("Arimaa");
        this.setLayout(new BorderLayout());
        this.setSize(950,700);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.menu = new Menu(this);
        this.setExtendedState(MAXIMIZED_BOTH);
        this.add(menu,BorderLayout.NORTH);
        this.setVisible(true);
    }

    public void setBoard(Board board) {
        if(this.board==null) {
            this.board = board;
            this.add(board, BorderLayout.CENTER);
        }
    }
    public void startSetUp(Boolean vsAi){
        this.setBoard(new Board());
        arimaaGame=new Game(vsAi, board,this);
        board.setGame(arimaaGame);
        arimaaGame.fillBoard();
        Player[] players = arimaaGame.getPlayers();
        this.menu.changeToPrepButtons();
        timerPanel=new TimerPanel(arimaaGame);
        arimaaGame.setTimerPanel(timerPanel);
        players[0].turnTimer=timerPanel.turnTimerGold;
        players[1].turnTimer=timerPanel.turnTimerSilver;
        players[0].setHasTurn(true);
        this.add(timerPanel,BorderLayout.WEST);
    }
    public void startGame(){
        Player[] players = arimaaGame.getPlayers();
        arimaaGame.setPreparationPhase(false);
        arimaaGame.setStartingFields();
        this.menu.changeToGameButtons();
        players[1].setHasTurn(false);
        players[0].setHasTurn(true);
        players[0].turnTimer.start();
    }
    public void endGame(){
        if(winnerScreen!=null){
            this.remove(winnerScreen);
        }
        this.remove(menu);
        this.remove(board);
        this.remove(menu);
        this.remove(timerPanel);
        this.revalidate();
        this.repaint();
        this.menu=new Menu(this);
        this.board=null;
        this.arimaaGame=null;
        this.timerPanel=null;
        this.winnerScreen=null;
        this.add(this.menu,BorderLayout.NORTH);
        this.revalidate();
        this.repaint();
    }
    public void changeToWinnerScreen(WinnerScreen winnerScreen){
        this.remove(board);
        this.revalidate();
        this.repaint();
        this.menu.changeToAfterWinButtons();
        this.winnerScreen=winnerScreen;
        this.add(winnerScreen,BorderLayout.CENTER);

    }

    public void openLastGame() {
        arimaaGame=GameLoader.loadGame();
        arimaaGame.setWindow(this);
        int silverElapsed=arimaaGame.getTimerPanel().turnTimerSilver.getElapsedTime();
        int goldElapsed=arimaaGame.getTimerPanel().turnTimerGold.getElapsedTime();
        this.setBoard(arimaaGame.getBoard());
        Player[] players=arimaaGame.getPlayers();
        for(Player player:players){
            player.turnTimer=new TurnTimer(player);
            if(player.isGold()) {
                player.turnTimer.setElapsedTime(goldElapsed);
            }else {
                player.turnTimer.setElapsedTime(silverElapsed);
            }
            player.turnTimer.updateTimeLabel();
        }
        timerPanel=new TimerPanel(players[0].turnTimer,players[1].turnTimer);
        arimaaGame.setTimerPanel(timerPanel);
        this.add(timerPanel,BorderLayout.WEST);
        menu.changeToPrepButtons();
        menu.changeToGameButtons();
        this.revalidate();
        this.repaint();
        if(arimaaGame.isVsAi()) {
            AIopponent aIopponent = ((AIopponent) arimaaGame.getPlayers()[1]);
            aIopponent.setPlayTimer(new Timer(3000, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    aIopponent.makePlay();
                }
            }));
            if (aIopponent.hasTurn()) {
                aIopponent.start();
                aIopponent.turnTimer.start();
            } else {
                aIopponent.pause();
                players[0].turnTimer.start();
            }
        }
        if (players[0].hasTurn()) {
            players[0].turnTimer.start();
        } else {
            players[1].turnTimer.start();
        }
    }
    public void saveGame(){
        GameSave gameSave = new GameSave(arimaaGame);
        try {
            FileOutputStream fileOut = new FileOutputStream("gameState.ser");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(gameSave);
            objectOut.close();
            System.out.println("Game saved successfully.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.endGame();
    }
    public  Game getArimaaGame() {
        return arimaaGame;

    }

    public Menu getMenu() {
        return menu;
    }
}
