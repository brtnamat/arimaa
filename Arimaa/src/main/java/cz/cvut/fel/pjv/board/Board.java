package cz.cvut.fel.pjv.board;

import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.pieces.Hare;
import cz.cvut.fel.pjv.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

public class Board extends JPanel implements ActionListener, Serializable {
    private static final char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
    private static final int[] nums = {1, 2, 3, 4, 5, 6, 7, 8};
    private static final Field[] fields=new Field[64];
    public Field[][] squares;
    private ArrayList<Field> traps;
    private Game game;


    public Board(){
        this.setVisible(true);
        this.setLayout(new GridLayout(8,8));
        squares=new Field[8][8];
        this.traps=new ArrayList<>();
        this.createFields();
    }
    public void createFields(){
        int i=-1;
        int column=-1;
        for(int number:nums){
            column++;
            int row=-1;
            for(char character:chars){
                row++;
                i++;
                Field newField;
                if((character=='c' || character=='f')&&(number==3||number==6)){
                    newField=new Field(character,number,true);
                    traps.add(newField);
                }else {
                    newField = new Field(character, number, false);
                }
                squares[row][column]=newField;
                newField.addActionListener(this);
                newField.setOpaque(true);
                if(newField.isTrap()){
                    newField.setBackground(Color.RED);
                    newField.setOriginalColor(Color.RED);
                }else if((column+row)%2==0 ) {
                    newField.setBackground(Color.WHITE);
                    newField.setOriginalColor(Color.WHITE);
                }else  {
                    newField.setBackground(Color.yellow);
                    newField.setOriginalColor(Color.yellow);
                }
                add(newField);
                fields[i]=newField;
            }
        }
        int fieldIndex=-1;
        for(Field field:fields){
            int neighbourIndex=0;
            fieldIndex++;
            if ((fieldIndex-8)>=0){
                Field westNeighbour=fields[fieldIndex-8];
                field.neighbours[neighbourIndex]=westNeighbour;
                field.setWestNeighbour(westNeighbour);
                neighbourIndex++;
            }
            if ((fieldIndex-1)>=0&& fieldIndex%8!=0 ){
                Field southNeighbour=fields[fieldIndex-1];
                field.neighbours[neighbourIndex]=southNeighbour;
                field.setSouthNeighbour(southNeighbour);
                neighbourIndex++;
            }
            if ((fieldIndex+1)<=63 && fieldIndex%8!=7){
                Field northNeighbour=fields[fieldIndex+1];
                field.neighbours[neighbourIndex]=fields[fieldIndex+1];
                field.setNorthNeighbour(northNeighbour);
                neighbourIndex++;
            }
            if ((fieldIndex+8)<=63){
                Field eastNeighbour=fields[fieldIndex+8];
                field.neighbours[neighbourIndex]=eastNeighbour;
                field.setEastNeighbour(eastNeighbour);
            }
        }
    }

    public void setGame(Game game) {
        this.game = game;
    }
    public void updateTraps(){
        for(Field trap:traps){
            if(trap.isOccupied()){
                Piece pieceOnTrap=trap.getOccupiedBy();
                pieceOnTrap.checkTrapped();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Field field=(Field) e.getSource();
        if(field.isTagged()){
            game.untagField(field);
            return;
        }
        if (!game.getPreparationPhase()){
            game.gameClickHandler(field);
        }else{
            game.preparationClickHandler(field);
        }
    }
}