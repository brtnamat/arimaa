package cz.cvut.fel.pjv;


import cz.cvut.fel.pjv.game.ApplicationWindow;


public class Main {
    public static void main(String[] args) {
        new ApplicationWindow();
    }
}