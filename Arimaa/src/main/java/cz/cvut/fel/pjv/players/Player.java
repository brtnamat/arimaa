package cz.cvut.fel.pjv.players;


import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.game.TurnTimer;
import cz.cvut.fel.pjv.pieces.Piece;

import java.io.Serializable;
import java.util.ArrayList;

public class Player implements Serializable {

    final private Boolean isGold;
    private int numOfTurnMoves;
    private boolean hasTurn;
    public ArrayList<Piece> allPieces=new ArrayList<Piece>();
    private final int startingHareRow;
    private int numOfRemainingHares=8;
    private final Game participatingGame;
    public TurnTimer turnTimer;

    public Player(boolean isGold,Game participatingGame) {
        this.isGold = isGold;
        this.hasTurn = false;
        this.participatingGame=participatingGame;
        if (isGold) {
            this.startingHareRow = 6;
        } else {
            this.startingHareRow = 1;
        }
    }


    public Game getParticipatingGame() {
        return participatingGame;
    }

    public ArrayList<Piece> getAllPieces() {
        return allPieces;
    }

    public boolean isGold() {
        return isGold;
    }

    public int getNumOfTurnMoves() {
        return numOfTurnMoves;
    }

    public int getStartingHareRow() {
        return startingHareRow;
    }

    public void setNumOfTurnMoves(int numOfTurnMoves) {
        this.numOfTurnMoves = numOfTurnMoves;
    }

    public boolean hasTurn() {
        return hasTurn;
    }

    public void setHasTurn(boolean hasTurn) {
        this.hasTurn = hasTurn;
    }

    public void decreaseNumOfHares(){
        numOfRemainingHares=numOfRemainingHares-1;
        if(numOfRemainingHares==0){
            if(isGold()) {
                participatingGame.lose(this, "Gold player lost all hares");
            }else {
                participatingGame.lose(this, "Silver player lost all hares");
            }
        }
    }
    public void addIntoAllPieces(Piece piece){
        this.allPieces.add(piece);
    }
    public void removeFromAllPieces(Piece piece){
        this.allPieces.remove(piece);
        if (piece.getType().equals("Hare")){
            this.decreaseNumOfHares();
        }
    }
    public void ranOutOfTime(){
        if(isGold){
            participatingGame.lose(this,"Gold player ran out of time");
        }
        else {
            participatingGame.lose(this,"Silver player ran out of time");
        }
    }

    public int getNumOfRemainingHares() {
        return numOfRemainingHares;
    }
}
