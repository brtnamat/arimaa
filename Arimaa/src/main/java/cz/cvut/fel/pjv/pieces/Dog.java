package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;

public class Dog extends Piece{
    private int dogNumber;


    public Dog(Player owner) {
        super(2, "Dog", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/wd.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/bd.png"));
        }
    }
}
