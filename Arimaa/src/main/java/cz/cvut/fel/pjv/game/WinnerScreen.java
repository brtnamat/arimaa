package cz.cvut.fel.pjv.game;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class WinnerScreen extends JPanel implements Serializable {
    public WinnerScreen(Boolean isGold, String winReason){
        this.setLayout(new BorderLayout());
        setBackground(Color.GREEN.darker());
        JLabel winnerLabel=new JLabel();
        if(isGold){
            winnerLabel.setText("Player 1 wins!! \n"+winReason);
        }else {
            winnerLabel.setText("Player 2 wins!! \n"+winReason);
        }
        winnerLabel.setHorizontalAlignment(JLabel.CENTER);
        winnerLabel.setFont(new Font("Arial", Font.BOLD, 24));
        winnerLabel.setForeground(Color.BLACK);
        add(winnerLabel,BorderLayout.CENTER);
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
    }
}
