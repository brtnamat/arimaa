package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.board.Field;
import cz.cvut.fel.pjv.game.Game;
import cz.cvut.fel.pjv.game.Move;
import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;

public class Piece implements Serializable {
    final private int strength;
    final private String type;
    final private Player owner;
    private Field occupiedField;
    private Field previousOccupiedField;
    private ImageIcon icon;
    private Field startingField;
    private ArrayList<Move> allMoves;


    public Piece(int strength, String type, Player owner) {
        this.strength = strength;
        this.type = type;
        this.owner = owner;
        allMoves=new ArrayList<Move>();
    }

    public ArrayList<Move> getAllMoves() {
        return allMoves;
    }

    public void setAllMoves(ArrayList<Move> allMoves) {
        this.allMoves = allMoves;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public void setStartingField(Field startingpField) {
        this.startingField = startingField;
    }

    public String getType() {
        return type;
    }

    public Player getOwner() {
        return owner;
    }

    public int getStrength() {
        return strength;
    }

    public Field getOccupiedField() {
        return occupiedField;
    }

    public void setOccupiedField(Field occupiedField) {
        this.occupiedField = occupiedField;
    }

    public Field getPreviousOccupiedField() {
        return previousOccupiedField;
    }

    public void setPreviousOccupiedField(Field previousOccupiedField) {
        this.previousOccupiedField = previousOccupiedField;
    }

    public void pull(Field newField, Piece pulledPiece, Game game) {
        if (getOwner().getNumOfTurnMoves() > 2) {
            return;
        }
        Player playingPlayer= this.getOwner();
        this.changeOccupation(newField);
        this.addIntoAllMoves();
        pulledPiece.changeOccupation(this.getPreviousOccupiedField());
        pulledPiece.addIntoAllMoves();
        playingPlayer.setNumOfTurnMoves(playingPlayer.getNumOfTurnMoves()+2);
        boolean pulledRemoved=pulledPiece.checkTrapped();
        if(pulledPiece.getType().equals("Hare") && !pulledRemoved){
            pulledPiece.hareAtTheEndCheck();
        }
        game.setEnemyField(null);
        game.setFieldToGoFrom(null);
        checkTrapped();
        game.checkChange(playingPlayer);
        game.checkIfAllAreFrozen();
    }

    public void push(Field newField, Piece pushedPiece, Game game) {
        if (getOwner().getNumOfTurnMoves() > 2) {
            return;
        }
        Player playingPlayer= this.getOwner();
        pushedPiece.changeOccupation(newField);
        pushedPiece.addIntoAllMoves();
        this.changeOccupation(pushedPiece.getPreviousOccupiedField());
        this.addIntoAllMoves();
        playingPlayer.setNumOfTurnMoves(playingPlayer.getNumOfTurnMoves()+2);
        boolean pushedRemoved=pushedPiece.checkTrapped();
        if(pushedPiece.getType().equals("Hare") && !pushedRemoved){
            pushedPiece.hareAtTheEndCheck();
        }
        game.setEnemyField(null);
        game.setFieldToGoFrom(null);
        checkTrapped();
        game.checkChange(playingPlayer);
        game.checkIfAllAreFrozen();
    }

    public void move(Field newField, Game game) {
        if (this.type.equals("Hare")){
            if(getOwner().getStartingHareRow()==6 && newField.getNum()>getOccupiedField().getNum()){
                return;
            } else if (getOwner().getStartingHareRow()==1 && newField.getNum()<getOccupiedField().getNum()) {
                return;
            }
        }
        Field fromField = this.getOccupiedField();
        fromField.setBackground(fromField.getOriginalColor());
        changeOccupation(newField);
        addIntoAllMoves();
        this.getOwner().setNumOfTurnMoves(this.getOwner().getNumOfTurnMoves() + 1);
        boolean trapped= checkTrapped();
        game.setFieldToGoFrom(null);
        game.checkChange(getOwner());
        game.checkIfAllAreFrozen();
        if(this.getType().equals("Hare") && !trapped){
            hareAtTheEndCheck();
        }

    }
    public boolean checkTrapped(){
        if (getOccupiedField().isTrap()) {
            for (Field field : getOccupiedField().getNeighbours())
                if (field!=null && field.isOccupied() && field.getOccupiedBy().getOwner() == getOwner()) {
                    return false;
                }
            removePiece();
            return true;
        }
        return false;
    }

    public void removePiece() {
        this.getOccupiedField().setOccupiedBy(null);
        this.setOccupiedField(null);
        this.setPreviousOccupiedField(null);
        this.getOwner().removeFromAllPieces(this);
    }

    public void changeOccupation( Field newField) {
        Field tempField = this.getOccupiedField();
        setPreviousOccupiedField(tempField);
        this.previousOccupiedField.setTagged(false);
        setOccupiedField(newField);
        newField.setOccupiedBy(this);
        this.previousOccupiedField.setOccupiedBy(null);
    }

    public boolean canMove() {
        boolean moveable=true;
        for (Field neighbourField : occupiedField.getNeighbours()) {
            Piece neighbourPiece=null;
            if(neighbourField!=null){
                neighbourPiece = neighbourField.getOccupiedBy();
            }
            if (neighbourPiece != null) {
                if (neighbourPiece.getOwner()!=this.getOwner() && neighbourPiece.getStrength() > this.getStrength()) {
                    moveable=false;
                }else if(neighbourPiece.getOwner()==this.getOwner()){
                    return true;
                }
            }
        }
        return moveable;
    }
    public void hareAtTheEndCheck(){
        if(this.getOwner().getStartingHareRow()==6 && this.getOccupiedField().getNum()==1){
            this.getOwner().getParticipatingGame().win(this.getOwner(),"Hare got to end");
        }
    }
    public void addIntoAllMoves(){
        this.allMoves.add(new Move(previousOccupiedField,occupiedField,this));
    }
}
