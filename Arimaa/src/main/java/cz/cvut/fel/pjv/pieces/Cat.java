package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;

public class Cat extends Piece{

    public Cat(Player owner) {
        super(1, "Cat", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/wc.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/bc.png"));
        }
    }
}
