package cz.cvut.fel.pjv.pieces;

import cz.cvut.fel.pjv.players.Player;

import javax.swing.*;
import java.awt.color.CMMException;

public class Camel extends Piece{
    static int camelNumber;

    public Camel(Player owner) {
        super(4, "Camel", owner);
        if (owner.isGold()){
            this.setIcon(new ImageIcon("pictures/wm.png"));
        }else {
            this.setIcon(new ImageIcon("pictures/bm.png"));
        }
    }

}
